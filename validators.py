from graphql import GraphQLError
from graphql_relay.node.node import from_global_id


def validate_name(value):
    """
    name validation
    """
    if len(value) < 5:
        raise GraphQLError("name should more than 5 letters")
    for i in range(len(list(value))):
        if value[i].isdigit():
            raise GraphQLError("use alphabatical letters only")
    return value


def validate_height(value):
    """
    height validation
    """
    if len(str(value)) != 3:
        raise GraphQLError('enter proper value')

    if not value.isdigit():
        raise GraphQLError('use digits only')
    return value


def validate_birth_year(value):
    """
    validate birth year
    """
    if not value.isdigit():
        raise GraphQLError('birth year-use digits only')

    if len(str(value)) != 4:
        raise GraphQLError('birth year not in proper format')

    return value


def validate_gender(value):
    """
    validate gender
    """
    if value != 'male' and value != 'female':
        raise GraphQLError('gender not in proper format')

    return value


def input_to_dictionary(input):
    """Method to convert Graphene inputs into dictionary"""
    dictionary = {}
    for key in input:
        # Convert GraphQL global id to database id
        if key[-2:] == 'id':
            input[key] = from_global_id(input[key])[1]
        dictionary[key] = input[key]
    return dictionary
